# glhf

GitLab Hook Forwarder: forward GitLab events to IRC.

## Usage

    glhf [options...] ircs://<username>:<password>@<host>

See `-help` for a full list of options.

Configure your HTTP reverse proxy to forward requests to glhf, then register
a new WebHook in the GitLab project settings.

Add `-gitlab-project <url>` to make the bot expand references like `#42`.

## License

MIT
