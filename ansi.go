package main

import (
	"fmt"
)

// https://modern.ircdocs.horse/formatting.html

type Color string

const (
	ColorGreen  Color = "03"
	ColorOrange Color = "07"
	ColorCyan   Color = "10"
)

func WithForeground(s string, color Color) string {
	return fmt.Sprintf("\x03%v%v\x03", color, s)
}
