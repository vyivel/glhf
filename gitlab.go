package main

import (
	"encoding/json"
	"fmt"
	"net/http"
	"net/url"
	"strings"
	"unicode"
)

type gitlabPushWebhook struct {
	Ref string `json:"ref"`

	Project struct {
		Name string `json:"name"`
	} `json:"project"`

	Commits []struct {
		ID     string `json:"id"`
		Title  string `json:"title"`
		Author struct {
			Name string `json:"name"`
		} `json:"author"`
	} `json:"commits"`
	TotalCommitsCount int `json:"total_commits_count"`
}

type gitlabIssueWebhook struct {
	Project struct {
		Name string `json:"name"`
	} `json:"project"`
	User struct {
		Username string `json:"username"`
	} `json:"user"`
	ObjectAttributes struct {
		Action string `json:"action"`
		Title  string `json:"title"`
		IID    int64  `json:"iid"`
		URL    string `json:"url"`
	} `json:"object_attributes"`
}

type gitlabMergeRequestWebhook struct {
	Project struct {
		Name string `json:"name"`
	} `json:"project"`
	User struct {
		Username string `json:"username"`
	} `json:"user"`
	ObjectAttributes struct {
		Action string `json:"action"`
		Title  string `json:"title"`
		IID    int64  `json:"iid"`
		URL    string `json:"url"`
	} `json:"object_attributes"`
}

type gitlabReleaseWebhook struct {
	Name    string `json:"name"`
	Project struct {
		Name string `json:"name"`
	} `json:"project"`
	URL    string `json:"url"`
	Action string `json:"action"`
}

type gitlabWikiPageWebhook struct {
	User struct {
		Username string `json:"username"`
	} `json:"user"`
	ObjectAttributes struct {
		Action  string `json:"action"`
		Title   string `json:"title"`
		Message string `json:"message"`
		URL     string `json:"url"`
	} `json:"object_attributes"`
}

type gitlabRef struct {
	Kind byte // # or !
	ID   string
}

func (ref *gitlabRef) String() string {
	return string(rune(ref.Kind)) + ref.ID
}

func findGitlabRefs(s string) []gitlabRef {
	var refs []gitlabRef
	m := make(map[gitlabRef]struct{})
	for s != "" {
		i := strings.IndexAny(s, "#!")
		if i < 0 {
			break
		}

		if i > 0 && unicode.IsLetter(rune(s[i-1])) {
			s = s[i+1:]
			continue
		}

		kind := s[i]
		j := i + 1
		for j < len(s) && unicode.IsDigit(rune(s[j])) {
			j++
		}
		if j < len(s) && unicode.IsLetter(rune(s[j])) {
			s = s[j:]
			continue
		}

		ref := gitlabRef{
			Kind: kind,
			ID:   s[i+1 : j],
		}
		s = s[j:]

		if _, dup := m[ref]; dup {
			continue
		}

		refs = append(refs, ref)
		m[ref] = struct{}{}
	}
	return refs
}

func fetchGitlabProjectID(path string) (int64, error) {
	resp, err := http.Get(gitlabURL + "/api/v4/projects/" + url.PathEscape(path))
	if err != nil {
		return 0, err
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		return 0, fmt.Errorf("HTTP error: %v", resp.Status)
	}

	var data struct {
		ID int64 `json:"id"`
	}
	if err := json.NewDecoder(resp.Body).Decode(&data); err != nil {
		return 0, err
	}
	return data.ID, nil
}

type gitlabIssue struct {
	Title  string `json:"title"`
	WebURL string `json:"web_url"`
}

func fetchGitlabIssue(id string) (*gitlabIssue, error) {
	endpoint := fmt.Sprintf("%v/api/v4/projects/%v/issues/%v", gitlabURL, gitlabProjectID, id)
	resp, err := http.Get(endpoint)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("HTTP error: %v", resp.Status)
	}

	var data gitlabIssue
	if err := json.NewDecoder(resp.Body).Decode(&data); err != nil {
		return nil, err
	}
	return &data, nil
}

type gitlabMergeRequest struct {
	Title  string `json:"title"`
	WebURL string `json:"web_url"`
}

func fetchGitlabMergeRequest(id string) (*gitlabMergeRequest, error) {
	endpoint := fmt.Sprintf("%v/api/v4/projects/%v/merge_requests/%v", gitlabURL, gitlabProjectID, id)
	resp, err := http.Get(endpoint)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("HTTP error: %v", resp.Status)
	}

	var data gitlabMergeRequest
	if err := json.NewDecoder(resp.Body).Decode(&data); err != nil {
		return nil, err
	}
	return &data, nil
}
